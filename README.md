`lunar-mode-line.el` is a tool to display lunar phase in the Emacs
mode line.

A [page on the EmacsWiki](https://www.emacswiki.org/emacs/LunarModeLine)
is dedicated to `lunar-mode-line.el`.
